# base image 
FROM node:12.18-slim

# set working directory
RUN mkdir /app
WORKDIR /app


#ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
#COPY public /usr/src/app/public
#COPY src /usr/src/app/src

COPY package.json /app/package.json
RUN npm install
COPY . .
#RUN npm install react-scripts -g

# start app
EXPOSE 8080
CMD ["npm","run", "serve"]
